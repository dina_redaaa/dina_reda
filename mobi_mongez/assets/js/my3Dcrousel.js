/**
 * Created by d3nq on 1/19/16.
 */
$(document).ready(function(){
    // with minimal configuration and default setting
    //	$('.jR3DCarouselGallery').jR3DCarousel({ slides: slides });

    // Or with options
    var slides = [{src: 'assets/images/iphone-1.jpg'},
        {src: 'assets/images/iphone-2.jpg'},
        {src: 'assets/images/iphone-3.jpg'},
        {src: 'assets/images/iphone-4.jpg'}];


    //for ( var i = 0; i < 4; i++) {
    //    slides.push({
    //        src: 'assets/images/slider1.jpg'})
    //}

    $('.jR3DCarouselGallery').jR3DCarousel({
        width:400,
        height: 222,
        slideLayout : 'cover',    // "contain" (fit according to aspect ratio), "fill" (stretches object to fill) and "cover" (overflows box but maintains ratio)
        animation: 'slide3D', // slide | slide3D | scroll | scroll3D | fade
        animationCurve: 'ease',
        animationDuration: 1000,
        animationInterval: 500,
        autoplay: true,
        onSlideShow: shown,		 // callback when Slide show event occurs
        navigation: 'circles', // circles | squares */
        slides: slides 			// array of images source or slides from template
    });

    function shown(slide){
        console.log("Slide shown: ", slide.find('img').attr('src'));
    }
});

