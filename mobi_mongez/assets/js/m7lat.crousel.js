/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    // with minimal configuration and default setting
    //	$('.jR3DCarouselGallery').jR3DCarousel({ slides: slides });

    // Or with options
    var slides = [{src: 'assets/images/samsung-1.jpg'},
        {src: 'assets/images/samsung-2.jpg'},
        {src: 'assets/images/samsung-3.jpg'},
        {src: 'assets/images/samsung-4.jpg'}];

    $('.jR3DCarouselGallery1').jR3DCarousel({
        //width:400,
        //height: 222,
        slideLayout : 'contain',    // "contain" (fit according to aspect ratio), "fill" (stretches object to fill) and "cover" (overflows box but maintains ratio)
        animation: 'slide3D', // slide | slide3D | scroll | scroll3D | fade
        animationCurve: 'ease',
        animationDuration: 1000,
        animationInterval: 500,
        autoplay: true,
        onSlideShow: shown,		 // callback when Slide show event occurs
        navigation: 'circles', // circles | squares */
        slides: slides 			// array of images source or slides from template
    });
    

    function shown(slide){
        console.log("Slide shown: ", slide.find('img').attr('src'));
    }
});




