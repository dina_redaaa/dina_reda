/**
 * Created by dina on 2/24/16.
 */
/*====================================
 VAGAS SLIDESHOW SCRIPTS
 ======================================*/
$.vegas('slideshow', {
    backgrounds: [
        { src: 'assets/images/bg/bg_1.jpg', fade: 1000, delay: 9000 },
        { src: 'assets/images/bg/bg_2.jpg', fade: 1000, delay: 9000 },
        { src: 'assets/images/bg/bg_3.jpg', fade: 1000, delay: 9000 },
        { src: 'assets/images/bg/bg_7.jpg', fade: 1000, delay: 9000 }
    ]
})('overlay', {
    /** SLIDESHOW OVERLAY IMAGE **/
    src: 'custom/vegas/overlays/06.png' // THERE ARE TOTAL 01 TO 15 .png IMAGES AT THE PATH GIVEN, WHICH YOU CAN USE HERE
});