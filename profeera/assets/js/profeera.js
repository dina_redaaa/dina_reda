/**
 * Created by d3nq on 1/8/16.
 */

$(document).ready(function () {
    /*====================================
     VAGAS SLIDESHOW SCRIPTS
     ======================================*/
    $.vegas('slideshow', {
        backgrounds: [
            {src: 'assets/images/bg_1.jpg', fade: 1000, delay: 9000},
            {src: 'assets/images/bg_2.jpg', fade: 1000, delay: 9000},
            {src: 'assets/images/bg_3.jpg', fade: 1000, delay: 9000}
        ]
    })('overlay', {
        /** SLIDESHOW OVERLAY IMAGE **/
        src: 'assets/vegas/overlays/06.png'
    });
});