/**
 * Created by d3nq on 1/5/16.
 */

(function ($) {
    if (document.documentElement.clientWidth > 800) {
        //if (window.innerWidth > 800){
        start();
    }
})(jQuery);

window.addEventListener("resize", function () {
    //location.reload();
    document.location.reload(true);
    start();
});

//$(window).resize(function() {
//    start();
//});

function start() {
    console.log(document.documentElement.clientWidth);
    if (document.documentElement.clientWidth > 800) {
        $.fn.menumaker = function (options) {
            var cssmenu = $(this), settings = $.extend({
                title: "Menu",
                format: "dropdown",
                sticky: false
            }, options);

            return this.each(function () {
                //cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
                $(this).find("#menu-button").on('click', function () {
                    $(this).toggleClass('menu-opened');
                    var mainmenu = $(this).next('ul');
                    if (mainmenu.hasClass('open')) {
                        mainmenu.hide().removeClass('open');
                    }
                    else {
                        mainmenu.show().addClass('open');
                        if (settings.format === "dropdown") {
                            mainmenu.find('ul').show();
                        }
                    }
                });

                if (settings.sticky === true) cssmenu.css('position', 'fixed');

                resizeFix = function () {
                    if ($(window).width() > 768) {
                        cssmenu.find('ul').show();
                    }

                    if ($(window).width() <= 768) {
                        cssmenu.find('ul').hide().removeClass('open');
                    }
                };
                resizeFix();
                return $(window).on('resize', resizeFix);

            });
        };
//})(jQuery);
//
//(function ($) {
        $(document).ready(function () {

            $(document).ready(function () {
                $("#cssmenu").menumaker({
                    title: "Menu",
                    format: "multitoggle"
                });

                $("#cssmenu").prepend("<div id='menu-line'></div>");

                var foundActive = false, activeElement, linePosition = 0, menuLine = $("#cssmenu #menu-line"), lineWidth, defaultPosition, defaultWidth;

                $("#cssmenu > ul > li").each(function () {
                    if ($(this).hasClass('active')) {
                        activeElement = $(this);
                        foundActive = true;
                    }
                });

                if (foundActive === false) {
                    activeElement = $("#cssmenu > ul > li").first();
                }

                defaultWidth = lineWidth = activeElement.width();

                defaultPosition = linePosition = activeElement.position().left;

                menuLine.css("width", lineWidth);
                menuLine.css("left", linePosition);

                $("#cssmenu > ul > li").hover(function () {
                        activeElement = $(this);
                        lineWidth = activeElement.width();
                        linePosition = activeElement.position().left;
                        menuLine.css("width", lineWidth);
                        menuLine.css("left", linePosition);
                    },
                    function () {
                        menuLine.css("left", defaultPosition);
                        menuLine.css("width", defaultWidth);
                    });

            });


        });
    }
}
