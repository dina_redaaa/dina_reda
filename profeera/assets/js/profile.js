/**
 * Created by dina on 2/23/16.
 */

document.getElementById("secondpersonal-info").style.display = 'none';
document.getElementById("address-info").style.display = 'none';
document.getElementById("skills-info").style.display = 'none';
document.getElementById("education-info").style.display = 'none';
document.getElementById("education-info").style.display = 'none';
document.getElementById("online-info").style.display = 'none';
document.getElementById("work-info").style.display = 'none';
document.getElementById("certificate-info").style.display = 'none';
document.getElementById("courses-info").style.display = 'none';

document.getElementById("btn-upload").style.display = 'none';


document.getElementById("personal-button").onclick = function () {
    if (document.getElementById("secondpersonal-info").style.display == 'block') {
        document.getElementById("secondpersonal-info").style.display = 'none'
    } else {
        document.getElementById("secondpersonal-info").style.display = 'block'
    }
    document.getElementById("firstpersonal-info").style.display = 'block';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';

};
//
document.getElementById("address-button").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if (
        document.getElementById("address-info").style.display == 'block') {
        document.getElementById("address-info").style.display = 'none';
    } else {
        document.getElementById("address-info").style.display = 'block';
    }
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
};
//
document.getElementById("skills-button").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if (
        document.getElementById("skills-info").style.display == 'block') {
        document.getElementById("skills-info").style.display = 'none';
    } else {
        document.getElementById("skills-info").style.display = 'block';
    }
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';

};
//
document.getElementById("education-button").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if (
        document.getElementById("education-info").style.display == 'block') {
        document.getElementById("education-info").style.display = 'none';
    } else {
        document.getElementById("education-info").style.display = 'block';
    }
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';

};

document.getElementById("course-button").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if (
        document.getElementById("courses-info").style.display == 'block') {
        document.getElementById("courses-info").style.display = 'none';
    } else {
        document.getElementById("courses-info").style.display = 'block';
    }
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
};

document.getElementById("online-button").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if(
    document.getElementById("online-info").style.display == 'block'){
        document.getElementById("online-info").style.display = 'none';
    }else {
        document.getElementById("online-info").style.display = 'block';
    }
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
};

document.getElementById("work-button").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if(
    document.getElementById("work-info").style.display == 'block'){
        document.getElementById("work-info").style.display = 'none';
    }else {
        document.getElementById("work-info").style.display = 'block';
    }
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
};

document.getElementById("certificate-button").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if(
    document.getElementById("certificate-info").style.display == 'block'){
        document.getElementById("certificate-info").style.display = 'none'
    }
    else {
        document.getElementById("certificate-info").style.display = 'block'
    }
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';

};



document.getElementById("personal-button-sm").onclick = function () {
    if (document.getElementById("secondpersonal-info").style.display == 'block') {
        document.getElementById("secondpersonal-info").style.display = 'none'
    } else {
        document.getElementById("secondpersonal-info").style.display = 'block'
    }
    document.getElementById("firstpersonal-info").style.display = 'block';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';

};
//
document.getElementById("address-button-sm").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if (
        document.getElementById("address-info").style.display == 'block') {
        document.getElementById("address-info").style.display = 'none';
    } else {
        document.getElementById("address-info").style.display = 'block';
    }
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
};
//
document.getElementById("skills-button-sm").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if (
        document.getElementById("skills-info").style.display == 'block') {
        document.getElementById("skills-info").style.display = 'none';
    } else {
        document.getElementById("skills-info").style.display = 'block';
    }
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';

};
//
document.getElementById("education-button-sm").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if (
        document.getElementById("education-info").style.display == 'block') {
        document.getElementById("education-info").style.display = 'none';
    } else {
        document.getElementById("education-info").style.display = 'block';
    }
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';

};

document.getElementById("course-button-sm").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if (
        document.getElementById("courses-info").style.display == 'block') {
        document.getElementById("courses-info").style.display = 'none';
    } else {
        document.getElementById("courses-info").style.display = 'block';
    }
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
};

document.getElementById("online-button-sm").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if(
        document.getElementById("online-info").style.display == 'block'){
        document.getElementById("online-info").style.display = 'none';
    }else {
        document.getElementById("online-info").style.display = 'block';
    }
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
};

document.getElementById("work-button-sm").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if(
        document.getElementById("work-info").style.display == 'block'){
        document.getElementById("work-info").style.display = 'none';
    }else {
        document.getElementById("work-info").style.display = 'block';
    }
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("certificate-info").style.display = 'none';
};

document.getElementById("certificate-button-sm").onclick = function () {
    document.getElementById("firstpersonal-info").style.display = 'block';
    if(
        document.getElementById("certificate-info").style.display == 'block'){
        document.getElementById("certificate-info").style.display = 'none'
    }
    else {
        document.getElementById("certificate-info").style.display = 'block'
    }
    document.getElementById("online-info").style.display = 'none';
    document.getElementById("courses-info").style.display = 'none';
    document.getElementById("education-info").style.display = 'none';
    document.getElementById("secondpersonal-info").style.display = 'none';
    document.getElementById("address-info").style.display = 'none';
    document.getElementById("skills-info").style.display = 'none';
    document.getElementById("work-info").style.display = 'none';

};


//JOB TITLE
document.getElementById("student-go").onclick = function () {
    document.getElementById("student-job").innerHTML = document.getElementById("studentjob-input").value;
    document.getElementById("studentjob-input").value = "";
    $(".collapse1").collapse('hide');

};

document.getElementById("student-job").onclick = function () {
    $(".collapse1").collapse();

};

//BAOUT
document.getElementById("about-go").onclick = function () {
    document.getElementById("student-about").innerHTML = document.getElementById("studentabout-input").value;
    document.getElementById("studentabout-input").value = "";
    $(".collapse2").collapse('hide');

};
//
document.getElementById("student-about").onclick = function () {
    $(".collapse2").collapse();

};

//FILE UPLOADER
$(function () {
    $("#upload_link").on('click', function (e) {
        e.preventDefault();
        $("#upload:hidden").trigger('click');
        document.getElementById("btn-upload").style.display = 'block'
    });
});

