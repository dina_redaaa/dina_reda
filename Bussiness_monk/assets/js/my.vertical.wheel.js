/**
 * Created by dina on 2/3/16.
 */

$('.box').FullPage({
    callback:function(i) {
        switch (i) {
            case 0:
                document.title = "Home";
                break;
            case 1:
                document.title = "Services";
                break;
            case 2:
                document.title = "Portfolio";
                break;
            case 3:
                document.title = "Comming Soon";
                break;
            case 4:
                document.title = "Team";
                break;
            case 5:
                document.title = "Contact";
                break;
            default:
                break;
        }
    }
});